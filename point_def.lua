local system = require("system");

-- This charges a given point to an address
function charge(address, point)
    local balance = system.getItem(address)
    if balance == nil then
        balance = 0
    end
    
	system.setItem(address, balance + point)
end

-- This returns current balance of a given address
function lookup(address)
    local res = system.getItem(address)
    return res
end

--[[This provide a functions to register a target return point.
    When from_address has a point more than this point, then 
    smart contract will return point to to_address]]
function registerReturn(my_address, to_address, target_return_point)
    local return_target_point_key, return_to_key = genReturnKeys(my_address)
    
    system.setItem(return_target_point_key, target_return_point)
    system.setItem(return_to_key, to_address)
end

-- This generates a key used to store return information
function genReturnKeys(address)
    returnTargetPointPrefix = 'TRG_'
    returnToPrefix = 'ADDR_'
    
    local return_target_point_key = returnTargetPointPrefix .. address
    local return_to_key = returnToPrefix .. address
    
    return return_target_point_key, return_to_key
end

-- This is used to transfer money from sender to receiver
function transfer(sender_address, receiver_address, amount)
    local sender_balance = system.getItem(sender_address)
    local receiver_balance = system.getItem(receiver_address)
    
    if sender_balance == nil then
        sender_balance = 0
    end
    if receiver_balance == nil then
        receiver_balance = 0
    end
    
    -- check sender's remaining
    if sender_balance - amount >= 0 then
        
        -- withdraw point    
        system.setItem(sender_address, sender_balance - amount)
        
        -- check return
        local return_target_point_key, return_to_key = genReturnKeys(receiver_address)
        local return_target_point = system.getItem(return_target_point_key)
        local return_to = system.getItem(return_to_key)
        
        -- when there's already registred return info
        if return_target_point ~= nil and return_to ~= nil and (return_target_point <=  receiver_balance + amount) then
            -- return point to target_to address
            local return_to_balance = system.getItem(return_to)
            
            if return_to_balance == nil then
                return_to_balance = 0
            end
            system.setItem(receiver_address, 0)
            system.setItem(return_to, return_to_balance + receiver_balance + amount)
        else
            -- send point to receiver
            system.setItem(receiver_address, receiver_balance + amount)
        end
        
    end
end