local system = require("system"); 

customer = "1NHgQgdhtPoWJuk6mNHfhDWcKssnA9puof"
shop = "13LCX2m4ke7ARcD7Zj14SxxSqBKXbfycMh"
bank = "1Kge7iM25USoeD1kwx2tKyZ3PoWc9meytw"

-- charge
ret, ok = call("charge", customer, 1000000) -- customer
assert(ok, ret)
ret, ok = call("charge", shop, 0) -- shop
assert(ok, ret)
ret, ok = call("charge", bank, 0) -- bank
assert(ok, ret)

-- lookup balance - Customer: 1000000, Shop: 0, Bank: 0
system.print("After Charge")
system.print("Customer: " .. call("lookup", customer) .. ", Shop: " .. call("lookup", shop) ..  ", Bank: " .. call("lookup", bank))


-- register return 100,000 point
ret, ok = call("registerReturn", shop, bank, 100000)
assert(ok, ret)


-- transfer 30,000 point
ret, ok = call("transfer", customer, shop, 30000)
assert(ok, ret)

-- lookup balance - Customer: 970000, Shop: 30000, Bank: 0
system.print("Transfer 30000")
system.print("Customer: " .. call("lookup", customer) .. ", Shop: " .. call("lookup", shop) ..  ", Bank: " .. call("lookup", bank))


-- transfer 70,000 point, will return 100,000 point to bank
ret, ok = call("transfer", customer, shop, 70000)
assert(ok, ret)

-- lookup balance - Customer: 890000, Shop: 0, Bank: 110000
system.print("Transfer 70000")
system.print("Customer: " .. call("lookup", customer) .. ", Shop: " .. call("lookup", shop) ..  ", Bank: " .. call("lookup", bank))